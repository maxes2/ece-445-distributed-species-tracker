const express = require('express');
const cors = require('cors');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server, {
  cors: {
    origin: 'http://localhost:3000',
    methods: ['GET', 'POST'],
    allowedHeaders: ['my-custom-header'],
    credentials: true,
  },
});
const port = 8000;

app.use(express.json());
app.use(cors());
app.use(express.urlencoded({ extended: true }));

let receivedMessages = [];

io.on('connection', (socket) => {
  console.log('A user connected');

  socket.on('disconnect', () => {
    console.log('A user disconnected');
  });
});

app.post('/api/messages', (req, res) => {
  const message = {
    id: Date.now(),
    text: req.body.message
  };
  receivedMessages.push(message);
  io.emit('message', message);
  res.status(200).send('Message received');
});

app.get('/messages', (req, res) => {
  res.json(receivedMessages);
});

server.listen(port, () => {
  console.log(`Server listening at http://localhost:${port}`);
});


// const express = require('express');
// const cors = require('cors');
// const app = express();
// const port = 8000;

// let receivedMessages = [];

// app.use(express.json());
// app.use(cors());
// app.use(express.urlencoded({ extended: true }));

// app.post('/api/messages', (req, res) => {
//   receivedMessages.push(req.body.message);
//   res.status(200).send('Message received');
// });

// app.get('/messages', (req, res) => {
//   res.send(`<pre>${receivedMessages.join('\n')}</pre>`);
//   // res.json({message: "Distributed Species Tracker"});
//   // res.json({ messages: receivedMessages });
// });

// app.listen(port, () => {
//   console.log(`Server listening at http://localhost:${port}`);
// });