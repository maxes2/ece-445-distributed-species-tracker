

2/2/2023

Today I plan on working on fleshing out the plan for how to implement the model that will be used on the microcontroller. The overall goal is to create a processing module that will take an image and output whether or not the target species is present in the image that was taken.

There are several choices that need to be made:
 - what species we will track (I am thinking of just tracking humans for now)
 - what image resolution should be supported
 - what model will be used
 - what dataset should be used

These choices are subject to some constraints:
 - the processing should preferrably take place fairly quickly
 - the model should not exceed the memory limits of the device (520 kB)
 - the model should be accurate (>75% on validation)
 - it should be easy to code the model and load the parameters on the microcontroller

These constraints naturally lead to the choice of training the model ourselves. This satisfies the contraints because:
 - we can make the model as small as we want, so the time that we take to process can be controlled. If we use any sort of pretrained model we cannot control how fast it is in any way
 - again, we can make the model as small as we want. 


2/8/2023

I am trying to decide if the camera subsystem should use a raspberry pi along with an FPC connected camera (https://www.amazon.com/Arducam-Megapixels-Sensor-OV5647-Raspberry/dp/B012V1HEP4/ref=asc_df_B012V1HEP4/?tag=&linkCode=df0&hvadid=385286500280&hvpos=&hvnetw=g&hvrand=6526311886520639882&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=9022185&hvtargid=pla-820020083673&region_id=674469&ref=&adgrpid=77282054583&th=1) or just using the microcontroller itself to process images and interface directly with a camera via spi (https://www.amazon.com/stores/Arducam/SPIcameraforArduino/page/213CA896-6E9A-4450-8218-3DA7C3F2C28B). 

Pros of using raspberry pi:
 - Seems much easier to set up the hardware
 - seems much easier to set up the software too
 - Can use a much larger model, possibly one with object detection
 - All past groups have done this so we can iterate on their work
 - Can even use python for image processing as opposed to a custom c++ library on the microcontroller which is  likely to be a pain
 - I could utilize my strengths of writing python and focusing on the actual imaging portion as opposed to having to figure out how to get spi or i2c working on the microcontroller and then on top of that having to write a driver for the camera and then writing a custom neural network module in c++

Pros of using microncontroller:
 - More difficulty points? (wouldn't even matter if we didn't get it working)
 - Costs less (a raspberry pi costs > $35 while spi camera costs about $25)
 - Sort of renders the mcu pointless because we could just use the PI for everything

Overall conclusion: There seems to be a very large margin in the difficulty of these implementations. The Pi implementation is not too difficult because many other groups have done it so we could look at their work, we could use other people's tools for neural network processing (such as pytorch), and most of the lower level software and hardware would be abstracted away as we could use drivers and spi/i2c communications that are built into the Pi. Meanwhile, the MCU driver implementation sounds very difficult because we have to write spi/i2c ourselves, we have to write a driver for the camera ourselves, no group has done this before so we are completely in the dark, we have to write an entire neural network library in c++, there is more limited constraints per the memory/clock time of the mcu, I don't really know anything about writing camrea drivers or performing image processing in c++, I don't really have the time required to learning all of these things and then implementing them as it seems to be quite a lot, the networking subsystem is already incredibly complicated and having many people working on it would be better.


3/2/2023

I have all three Jetson Nanos from Gruev, and yesterday and today I am working on turning them on. The correct funcionality of the Jetson Nano is as follows:
 - when you insert a power source, the green LED is supposed to light up
 - When you insert a power source and show the output on the screen, then the screen should read Nvidia
 - when you insert a power source and a video cable, and then insert an SD card with Linux on it (booted like this: https://developer.nvidia.com/embedded/learn/get-started-jetson-nano-devkit#prepare)

 My functionality:
 - Board #1 does not light up at all when micro USB is plugged in
 - Board #2 lights up but does not show up on vga to hdmi on lab computers
 - Board #3 lights up but does not show up on vga to hdmi on lab computers

Now board #2 lights up but turns off when the SD card is put in.

3/20/2023:

I managed to get the Jetsons working and today I am trying to get PyTorch working. It requires very specific packages of torch and torchvision, which I will download today. If this does not work, then fortunately the Jetson provides many other APIs for object detection, but since this is the API that I am the most familiar with I think it is the best course of action.

3/21:

After finding the correct packages, I have installed torch with Cuda, but every time I allocate any size tensor to Cuda, then the system adds 1M of memory, which slows down the computer and doesn't leave enough room for a model. So I think that I will either need to use their docker image which is even more specific to the Nano or I will have to use a higher-level wrapper. I will follow this course to see how they recommend doing it: https://courses.nvidia.com/courses/course-v1:DLI+S-RX-02+V2/.

3/24:

After doing the first few lessons of the course, I managed to set the swap memory to 4 MB by just following this video: https://www.youtube.com/watch?v=uvU8AXY1170, and I managed to run object detection at a very slow rate. When I use the docker image from the course, then once again, the system adds far too much memory than needed and the system slows down and there is not enough room for the model. But this method does actually work so if we cannot get any other setup working then this should be sufficient for at least some functionality.

3/25:

I have finally solved this bug and gotten a model loaded onto the system. To solve, I just built jetson inference from source (https://github.com/dusty-nv/jetson-inference/blob/master/docs/building-repo-2.md), and modified the detection script that they provide. This solution did not even require PyTorch at all, and I believe that this software is specially created to run on the Jetson, making it an even better solution than what I tried before. Now our system can do object detection, and I will move onto using the PIO pins.

3/26:

I have gotten simple PIO input and output working by changing the permission levels of the PIO pins and using the provided examples in the Jetson directory. The interface is the same as a Raspberry Pi and is very simple to get set up.

3/28:

We are working on connecting the Jetson to the MCU through the PIO pins and we have found that there is a minimum of 1s needed for the Jetson to recognize an input signal from the MCU and vice versa. This will become a bottleneck in our final system, because we will not be able to capture and process the images as fast as we had hoped. We will have to modify our requirement of capturing and processing in 100 ms to 2 s to make up for this. In addition, we have found that SPI will not work as an interface between the Jetson and the MCU, because the Jetson and MCU are oth configured to be in master mode in SPI. Thus, we have decided to create our own protocol where the MCU uses one wire to tell the Jetson to take and process an image and the Jetson uses two wires to tell the MCU whether or not a human was present in the photograph.

4/15:

We have finished all parts of the project and we are running several demos. Our demo consists of setting up the three nodes and then triggering the different sensors and sending positive and negative classifications as needed. We will also run the experiments for model classification and accuracy today as well.