# Jonathan Notebook

- [Jonathan Notebook](#jonathan-notebook)
- [2023-01-31 - General Meeting Updates](#2022-01-31---general-meeting-updates)
- [2023-02-02 - Initial Block Diagram](#2022-02-02---initial-block-diagram)
- [2023-02-06 - Researching Power Submodule and Discussion with Jason](#2023-02-06---researching-power-submodule-and-discussion-with-jason)
- [2023-02-13 - More Power Management Discussion](#2023-02-13---More-Power-Management-Discussion)
- [2023-02-15 - Hardware Design Discussion](#2023-02-15---Hardware-Design-Discussion)
- [2023-02-20 - PIR Sensor](#2023-02-20---PIR-Sensor)
- [2023-03-03 - PCB Design](#2023-03-03---PCB-Design)
- [2023-03-04 - PCB Design](#2023-03-04---PCB-Design)
- [2023-03-05 - PCB Design](#2023-03-05---PCB-Design)
- [2023-03-06 - PCB Design](#2023-03-06---PCB-Design)
- [2023-03-08 - PCB Order and OpenCV with Jetson](#2023-03-08---PCB-Order-and-OpenCV-with-Jetson)
- [2023-03-21 - Initial CAD Design](#2023-03-21---Initial-CAD-Design)
- [2023-03-26 - Tests and Verifications for Battery Charging Circuit](#2023-03-26---Tests-and-Verifications-for-Battery-Charging-Circuit)
- [2023-04-01 - Soldering PCB Board](#2023-04-01---Soldering-PCB-Board)
- [2023-04-03 - Flashing Attempt Via UART with Breakout Board](#2023-04-03---Flashing-Attempt-Via-UART-with-Breakout-Board)
- [2023-04-09 - Flashing Attempt Via UART with the PCB](#2023-04-09---Flashing-Attempt-Via-UART-with-the-PCB)
- [2023-04-10 - Flashing Attempt Via JTAG](#2023-04-10---Flashing-Attempt-Via-JTAG)
- [2023-04-11 - Solder More PCB Boards](#2023-04-11---Solder-More-PCB-Boards)
- [2023-04-12 - Battery Management Circuit Tests](#2023-04-12---Battery-Management-Circuit-Tests)
- [2023-04-17 - Solar Panel Tests](#2023-04-17---Solar-Panel-Tests)


# 2023-01-31 - General Meeting Updates

During today's meeting, we experimented with the ESP32s3 microcontroller and dev board. We were able to successfully flash an external LED using one of the GPIO pins instead of the built in LED via SMT. It was difficult to actually observe the flashing LED as the voltage is regulated to 3.3V. We will need to keep track of the voltage and current requirements of each submodule to ensure that the power system can support what is necessary

# 2023-02-02 - Initial Block Diagram 

During today's meeting, we established a triweekly team meeting schedule and worked on our high level understanding of each submodule. We decided that we can keep a node locally to act as a central node for the "on field" nodes to communicate via LORA and the user can access data from the network fro mthe central node via USB or WIFI. Reducing the need for a constant WIFI network dramatically reduces the power usage.

![](Senior_Project_Block_Diagram.png)

# 2023-02-06 - Researching Power Submodule and Discussion with Jason

I have limited background knowledge regarding the battery/power requirements of our project so I spent some time researching common battery solutions for the ESP32 microcontroller. [This Video](https://youtu.be/FrCgQgahzsI)  provided 5 examples of possible battery solutions for 24/7 devices, event triggered devices, etc. I initally thought that we may be able to use an event triggered technique which would allow the microcontroller to be in a deep sleep state for the majority of the time, but that would interfere with our networking component between nodes. Our device is therefore 24/7 and will need a long lasting battery. 

Possible Solutions include

 - Basic rechargable battery
    - will require a recharging circuit which may overcomplicate our module
    - battery life deteriorates after # recharges
    - will need to design a weather/environment proof recharging port
 - LiFePo4 Battery
    - Lithium Iron Phosphate Batteries are rated at 3.3 V +/- 0.1V, exactly what is needed for the ESP32 microcontroller
    - lasts for 10+ years
    - will require a custom battery encasing
    - need to figure out current output
    - rechargeable and can be safely disposed (non-toxic for environment)
 - 3 AAA Batteries
    - Readily available solution
    - will require a voltage regulator to bring down from 4.6V -> 3.3V
    - Does not last as long as LiFePo4 Battery
    - cheaper solution than LiFePo4 Battery
    - rechargeable and can be safely disposed (non-toxic for environment)

Had a quick discussion with Jason about Antenna choices for both the LoRa module and GPS module. He suggested that we should not go with inexpensive GPS chip options and overall the antenna situation with GPS may be very finicky. This will be an area of concern.

# 2023-02-13 - More Power Management Discussion

Thinking of more practical solutions for the power management situation of our project. We need to provide 3.3 V +/- 0.1 V as well as support current draw up to 1.5A while lasting for a significant number of hours of continuous use to show the practicality of our project. Ideally the battery should last for more than 24 hours, hopefully ~ 1 week.

If we go the disposable battery route, we would need a LiFePo4 3.3V Battery with 50+ Ah which is a very large and clunky battery. This is not very practical.

An alternative solution would to have a solar powered rechargeable circuit with a smaller battery. The TP4056 Lithium Battery Charging Board would regulate charging and discharging of the battery and the solar panel.

Discussion with Jason suggested that we would need a MPPT and Switch Mode IC for the solar panel system which would allow us to maximize the output from the solar panels. he also suggested going with a Lithium ion battery instead of LiFePo4 battery. Do Not do a linear power supply as it will waste a lot of power.

# 2023-02-15 - Hardware Design Discussion

Met with Ryan to discuss the hardware components relating to the RFM95W and the GPS Module. While Ryan and I stepped through the RFM95W module code to debug the transmitter/receiver handler, we discussed possible orientations of the PCB that would fit our design as well as the cutout wholes and positions of the GPS and LORA antennas.

# 2023-02-20 - PIR Sensor

Met with Ryan to experiment with the PIR Sensor. We used the PIR Sensor to trigger an LED. The Sensor seems to only trigger on living beings (objects with a heat sense) because it was not triggered when we rolled a chair or other objects in front of it. The sensor is pretty sensitive and has a wide range in which it is triggered and we may need to adjust this for the camera module. Another possible problem is that the LED was barely powered so the signal wire from the PIR sensor may not be providing sufficient voltage for a GPIO pin of the esp32 microcontroller.

# 2023-03-03 - PCB Design

Met with Ryan to begin the layout of our PCB design in KiCAD. We selected and placed all of our components in the schematic and ensured that our deisng for the power module would be sufficient for our use case. We began discussing the placement of footprints on the actual PCB but did not start routing any traces.

# 2023-03-04 - PCB Design

Continued to work on the PCB design in KiCAD. Ryan and I spent the majority of the time discussing and confirming the placement of the MCU, radio modul, GPS, and USB port in relation to one enother to ensure the proper routing of power lines, data lines and transmission lines while staying within the PCB requirements. This included finding placement and routes for transmission lines, SPI lines so that these data lines can run with an uninterrupted ground plane and had matching lengths. It was extremely difficult to fit within the requirements but we found a workaround since the only time data would be flowing in the USB data lines is when the board is flashed and none of the other lines will be transmitting any data. 50% of the board was completed


# 2023-03-05 - PCB Design

Continued to work on the PCB design in KiCAD. Ryan and I spent the majority of the time routing the rest of the PCB, working on the bottom half of the pCB with the battery management module and boost converter and voltage regulators. 100% of the board was completed including routing and route widths.


# 2023-03-06 - PCB Design

Ryan realized an important factor that the power module design would be insufficient for supplying enough current at 3.3V for the MCU, RFM, and GPS modules. This was due to a miscalculation of the MCU's current draw. It regulates at 3.3V but has a dropout voltage of 0.65V at 0.5A of current draw.  We can't afford 
that much dropout since we're supplying the regulator with 3.7V and would need about 0.8A of current draw at peak times. Ryan suggested a possible solution by having 2 regulators, one regulator will be feeding into the MCU, and the other regulator will feed into the Radio, GPS, and PIR sensor to support the current draw of all of the components. We made these modifications to the PCB and finished routing all of the traces. We then went through the PCB checklist and audit and tweaked layout and addressed DRC warnings. The gerber files were submitted onto PCBway and passed the audit. We have finished our first iteration of our PCB Design.

# 2023-03-08 - PCB Order and OpenCV with Jetson

Performed final checkup of the PCB Design and submitted gerber files to TA for the first PCB order. Met up with Ryan and Max to begin working with the Jetson Nano. After learning how to boot up the Jetson, we installed all of the necessary software to use OpenCV. We finally wrote a simple python script that can read directly from a USB webcam. This step was extremely important to start the image classification process

# 2023-03-21 - Initial CAD Design

Began initial iteration of CAD Design of the physical design of the system. The Jetson Nano is the largest component in our design at 69.6 mm x 45 mm. We designed the PCB to be exactly the same size so we essentially need a rectangular prism with cutout holes for the PIR sensor, camera, GPS antenna, and RFM antenna. We currently do not know the size of the camera so I made a rectangular prism 75 mm x 50 mm x 50 mm. We may need to raise the height extrusion to make more room for heat dissipation of the Jetson and miscellaneous wiring. I am worried about the heat dissipation because the heat sink on the Jetson is quite bulky and there should not be anything in contact with the sink to dissapate correctly. Need to find a balance between ample ventilation throughout the system and weatherproofing of the exterior.

# 2023-03-26 - Tests and Verifications for Battery Charging Circuit

The first test and verification is for the battery charging circuit. According to the complete charge cycle provided by the TP4056 datasheet, it takes approximately 1.25 hours to nearly fully charge a 1000mAh battery.

![](Charge_Cycle.png)

Using the formula and estimating that battery charging will result in a maximum of 40% losses we can estimate the time to fully charge the 10050mAh 3.7V battery with a charging voltage of 4.2V and charging current of 1A to be 14.07 hours. To test this, we will charge and discharge the battery 10 times. Using the LEDs on the circuit, we can determine when the  battery is fully charged. We will then take the average of the 10 tests and perform error analysis in comparison to the calculated charging time. We can also simulate the battery capacity in relation to the system by discharging the battery with a current draw of 2.5 A. Next, the voltage regulators and boost converters must be tested to see if it can support the current draw requirements of our system. Testing and verifying this includes supplying the chips with a steady voltage supply from a power supply and probing the output to verify 3.3V and 5V output respectively. We can also simulate a current load of 2.5A and ensure that the voltage does not drop below the voltage requirements. If these tests are verified, the power management system is capable of supporting the entire node system.


# 2023-04-01 - Soldering PCB Board

Worked with Ryan to solder the first iteration of the PCB Board. We discussed which components we wanted to use the oven for and which components to hand solder. We decided to use the stencil and solder paste for the finer pitched footprints and baked the board with the oven. It took several bakes to get the components aligned because of solder bridging and the solder paste drying out. The USB footprint proved to be very difficult to solder correctly because of its very fine pitched contact points and those points were under the usb encasing, making it very hard to fix with flux. We then tried flashing the ESP32 MCU via the USB data lines but the computer would not recognize the chip as a device. We probe the PCB with the multimeter and confirmed that the MCU was bring powered and drawing current. The USB was also being powered correctly, but probing the data lines showed that nothing was being sent to the MCU. After talking to another group that was using a similar ESP32 MCU, we decided to order a UART to USB bridge to try to flash the MCU in a different way.

# 2023-04-03 - Flashing Attempt Via UART with Breakout Board

Using the UART to USB bridge, we began trying to flash our ESP32 Breakout Board with a simple LED blinking program. We eventually got the blinking LED program to flash via UART bridge, but we encountered invalid packet protocols when we attempted to flash the board with more complex programs such as our networking program.

# 2023-04-09 - Flashing Attempt Via UART with the PCB

Met with Ryan to try to directly flash the MCU on the PCB using the UART bridge instead of the microUSB on the PCB. Decided to unsolder the MCU and LoRa module that we baked on the PCB previously and resolder a new ESP32 chip to eliminate the possibility of a faulty MCU due to the number of times we needed to bake the PCB the first time. I soldered the ESP32 and LoRA module by hand to prevent any issues occurring with the high temperatures that the oven needs to reach to achieve reflow temperature. After fixing bridging with flux and Jason showing us how to properly clean off the pads, we were sure that there were no soldering issues with the board. The computer was able to recognize a valid serial power with the UART bridge, but we were still unable to flash the board due to more inalid packet header errors. Ryan continued to try to debug the system and implemented a pull-up resister circuit to manually drive some of the logic headers high and hard code values. Despite these efforts, we were still unable to flash the MCU.

# 2023-04-10 - Flashing Attempt Via JTAG

Attempted another possible flashing solution using a JTAG debugger board. Met with Ryan and Jason to try this method but ultimately did not work either. With this, Jason suggested that we move on from our attempts with flashing the MCU on the PCB to working with what we have on the breakout boards.

# 2023-04-11 - Solder More PCB Boards

Worked with Ryan to solder all of the components on 2 more PCB boards. Worked extensively to make sure that nothing was bridging and soldered the majority of the components by hand to prevent any issues occurring with the high reflow temperature point of the oven. The boost converter circuit IC proved to be impossible to solder even with the oven because the contact points were so fine pitched (even more than the microUSB) and laid under the IC making it impossible to fix any bridging issues with flux. We decided to move on from the boost converter circuit and power the jetson externally. We also tried flashing the 2 new PCBs and were still unsuccessful. However, we ran some initial tests of the battery management circuit by powering via power supply (3.7V) and probing contact points with the multimeter. The tests were promising as the voltage and circuit readings were as expected.

# 2023-04-12 - Battery Management Circuit Tests

Worked with Ryan to verify more of the battery management circuit tests. With the 3.7V LiPo batteries, we were successfully able to charge the LiPo battery via external power supply. The batteries charge at 1A of current which was programmed via a 1.2k resistor within the TP4056 schematic. The status LEDs also lit up correctly to signal whether the battery was currently charging or fully charged. The battery supplies 3.7V which is regulated to 3.3V to power the rest of the circuit. The next step will be testing the charging circuit with the solar panel rather than an external power supply.

Discussed possible UI options of the website with Ryan and Max to display the published dta of the network. Ryan and Max were able to set up a simple server from the ESP32.

# 2023-04-17 - Solar Panel Tests

Ryan helped me setup the solar panels with the battery charging circuit. The solar panels are rated to 5.5V which should be sufficient to charge the batteries as they need 4.2V to charge the 3.7V batteries. Ryan was able to confirm that the batteries charge by shining a flashlight at the solar panels to generate over 5V. Ryan was also able to solder the solar panel and battery connections and ensure that one node's control system could be powered entirely from the solar panel setup. While the battery has a lower mAh capacity than we initially anticipated, we were able to show in demo purposes that our system can be sustained for an extended period of time with the battery constantly charging through the solar panel and discharging to power the circuit.

