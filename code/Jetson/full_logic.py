import RPi.GPIO as GPIO
import time
from jetson_inference import detectNet
from jetson_utils import videoSource, videoOutput, Log

input = "/dev/video0"
network = "ssd-mobilenet-v2"
threshold = 0.5

input = videoSource(input)#, argv=sys.argv)
net = detectNet(network, threshold)#args.network, sys.argv, args.threshold)

idx_to_class = []
fname = "classes.txt"
file = open(fname, 'r')
idx_to_cls = [line.strip() for line in file.readlines()]
print(idx_to_cls)

input_pin = 12
output_pin1 = 13
output_pin2 = 11
GPIO.setmode(GPIO.BOARD)
GPIO.setup(input_pin, GPIO.IN)
GPIO.setup(output_pin1, GPIO.OUT, initial=GPIO.LOW)
GPIO.setup(output_pin2, GPIO.OUT, initial=GPIO.LOW)

# func to output: GPIO.output(output_pin, value)
# func to read input: value = GPIO.input(input_pin)

while True:
    if GPIO.input(input_pin):
        print("Received message!!!!")
        time.sleep(3)

        # check if person in image
        img = input.Capture()

        if img is None: # timeout
            print("No image found")
            GPIO.output(output_pin2, GPIO.HIGH)
            time.sleep(1)
            GPIO.output(output_pin2, GPIO.LOW)
            continue

        detections = net.Detect(img,overlay="box,labels,conf")
        found_person = False
        for d in detections:
            if d.ClassID == 1:
                found_person = True

        
        if found_person:
            print("positive")
            GPIO.output(output_pin1, GPIO.HIGH)
            time.sleep(1)
            GPIO.output(output_pin1, GPIO.LOW)
        else:
            print("negative")
            GPIO.output(output_pin2, GPIO.HIGH)
            time.sleep(1)
            GPIO.output(output_pin2, GPIO.LOW)
    else:
        time.sleep(1)

    # exit on input/output EOS
    # if not input.IsStreaming() or not output.IsStreaming():
    #     break

GPIO.cleanup()
