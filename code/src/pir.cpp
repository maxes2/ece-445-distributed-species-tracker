#include <Arduino.h>
#include <SPI.h>


#define IR_PIN 5
#define RGB_LED 48

// #define JETSON_MOSI 37
// #define JETSON_MISO 39
// #define JETSON_SCK 40
// #define JETSON_CS 41

#define JETSON_OUT 37  // J12
#define JETSON_YES 39  // J13
#define JETSON_NO 41   // J11

int counter = 0;

// void setup() {
//     Serial.begin(9600);
//     delay(1000);
//     if (Serial){
//         neopixelWrite(RGB_BUILTIN, 0,0,255); // blue led if serial initialized
//         delay(1000);
//         neopixelWrite(RGB_BUILTIN, 0,0,0);
//         delay(1000);
//     }
//     // pinMode(IR_PIN, INPUT);
// }

// void loop() {
//     pinMode(IR_PIN, INPUT);
//     if (digitalRead(IR_PIN) == HIGH) {
//         pinMode(IR_PIN, OUTPUT);
//         Serial.print("Motion detected #");
//         Serial.println(counter);
//         counter++;
//         neopixelWrite(RGB_LED, 0,255,0); //message sent
//         delay(100);
//         neopixelWrite(RGB_BUILTIN, 0,0,0);
//     } else {
//         neopixelWrite(RGB_LED, 0,0,0);
//     }
//     delay(4000);
// }

void setup() {
    Serial.begin(9600);
    delay(1000);
    if (Serial){
        neopixelWrite(RGB_BUILTIN, 0,0,255); // blue led if serial initialized
        delay(1000);
        neopixelWrite(RGB_BUILTIN, 0,0,0);
        delay(300);
    }

    int count = 0;

    pinMode(JETSON_NO, INPUT);
    pinMode(JETSON_YES, INPUT);
    pinMode(JETSON_OUT, OUTPUT);

    pinMode(IR_PIN, INPUT);
    digitalWrite(IR_PIN, LOW);

    while(1) {
        if (digitalRead(IR_PIN) == HIGH) {
            Serial.print("Motion detected!!!  ");
            Serial.println(count++);
            // digitalWrite(JETSON_OUT, LOW);
            // Serial.println("Lowered pin");
            // delay(300);
            // digitalWrite(JETSON_OUT, HIGH);
            // Serial.println("Raised pin");
            // delay(1000);
            // digitalWrite(JETSON_OUT, LOW);
            delay(1000);

            // int counter = 0;
            // while(1) {
            //     if (digitalRead(JETSON_YES) == HIGH) {
            //         Serial.println("Pos classification!!");
            //         counter++;
            //         delay(1000);
            //         break;
            //     }
            //     else if (digitalRead(JETSON_NO) == HIGH) {
            //         Serial.println("Neg classification");
            //         counter++;
            //         delay(1000);
            //         break;
            //     }

            //     // if (counter >= 10) {
            //     //     break;
            //     // }
            // }
        }

    }
    // digitalWrite(JETSON_OUT, LOW);

    // delay(1000);
    // digitalWrite(JETSON_OUT, HIGH);
    // Serial.println("Raised pin");
    // delay(5000);

    // SPI.begin(JETSON_SCK, JETSON_MISO, JETSON_MOSI, JETSON_CS);
    // Serial.println("Begain SPI");

    // digitalWrite(JETSON_CS, LOW);
    // delay(100); // Give the Jetson Nano some time to get ready

    // // Request image
    // SPI.transfer(0xFF); 
    // Serial.println("Sent message");
    // delay(100);

    // // Receive response from Jetson Nano
    // byte response = SPI.transfer(0x00); 
    // digitalWrite(JETSON_CS, HIGH);

    // // Process response
    // Serial.print("Received response: ");
    // Serial.println(response, HEX);

    // vTaskDelay(3000);
}

void loop() {
    // digitalWrite(JETSON_CS, LOW);
    // delay(100); // Give the Jetson Nano some time to get ready
    // // Receive response from Jetson Nano
    // byte response = SPI.transfer(0x00); 
    // digitalWrite(JETSON_CS, HIGH);

    // // Process response
    // Serial.print("Received response: ");
    // Serial.println(response, HEX);

    // if (digitalRead(JETSON_OUT) == HIGH) 
    //     Serial.println("Pos classification");
    // if (digitalRead(JETSON_OUT == LOW))
    //     Serial.println("Pos classification");
}