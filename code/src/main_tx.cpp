#include <Arduino.h>
#include <SPI.h>
#include <RH_RF95.h>

#define RFM95W_RST 35
#define RFM95W_EN 1
#define RFM95W_INT 14
#define RFM95W_CS 10

#define RGB_LED 48
#define RFM95W_FREQ 915.0


// bool pic_taken = false;

// struct sighting_data {
//     double gps_lat;
//     double gps_long;
//     unsigned long time_stamp;
//     char animal[9] = "SQUIRREL";
// };

// TaskHandle_t Task1;
// TaskHandle_t Task2;

int16_t packetnum = 0;

// void threadOne(void * pvParameters){
//     while (!rfm95w2.init()) {
//         Serial.println("First Radio Initialization Failed");
//         neopixelWrite(RGB_LED, 255,0,0); //red led if radio failed
//         delay(1000);
//         neopixelWrite(RGB_LED, 0,0,0);
//         while (1);
//     }
//     // neopixelWrite(RGB_BUILTIN, 0,255,0); //green led if radio success
//     // delay(1000);
//     // neopixelWrite(RGB_BUILTIN, 0,0,0);
//     Serial.print("First Radio Initialized from core: ");
//     Serial.println(xPortGetCoreID());

//     if (!rfm95w2.setFrequency(RFM95W_FREQ)) {
//         Serial.println("First setFrequency Failed");
//         while (1);
//     }
//     Serial.print("First frequency set to: "); Serial.println(RFM95W_FREQ);

//     rfm95w2.setTxPower(23, false);

//     delay(3000);
//     int counter = 0;
//     for (;;) {
//         Serial.print("Sending message to rf95 server from core: ");
//         Serial.println(xPortGetCoreID());
//         char radiopacket[20] = "Hello World #      ";
//         itoa(packetnum++, radiopacket+13, 10);
//         Serial.print("Sending "); Serial.println(radiopacket);
//         radiopacket[19] = 0;
        
//         Serial.println("Sending..."); delay(10);
//         rfm95w2.send((uint8_t *)radiopacket, 20);

//         Serial.println("Waiting for packet to complete..."); delay(10);
//         rfm95w2.waitPacketSent();
//         Serial.println("PACKET SENT");
//         // Now wait for a reply
        
//         // uint8_t buf_tx[RH_RF95_MAX_MESSAGE_LEN];
//         // uint8_t len = sizeof(buf_tx);
//         // Serial.println("Waiting for reply..."); delay(10);
//         // if (rfm95w2.waitAvailableTimeout(30000))
//         // { 
//         //     Serial.print("Going to try to receive the reply from core: ");
//         //     Serial.println(xPortGetCoreID());
//         //     // Should be a reply message for us now   
//         //     if (rfm95w2.recv(buf_tx, &len))
//         //     {
//         //         Serial.println("GOT A REPLY HOLY SHIT: ");
//         //         Serial.println("!!!!!!!!!!!!!");
//         //         Serial.println("!!!!!!!!!!!!!");
//         //         Serial.println("!!!!!!!!!!!!!");
//         //         Serial.println((char*)buf_tx);
//         //         Serial.println("!!!!!!!!!!!!!");
//         //         Serial.println("!!!!!!!!!!!!!");
//         //         Serial.println("!!!!!!!!!!!!!");
//         //         Serial.print("RSSI: ");
//         //         Serial.println(rfm95w2.lastRssi(), DEC);    
//         //     }
//         //     else
//         //     {
//         //         Serial.println("Receive failed");
//         //     }
//         //     break;
//         // }
//         // else
//         // {
//         //     Serial.println("No reply, is there a listener around?");
//         // }
//         break;
//     }
    // for (;;) {
    //     counter++;
    //     counter--;
    //     delay(1000);
    // }
// }

// void threadTwo(void * pvParameters){

//     while (!rfm95w.init()) {
//         Serial.println("Second Radio Initialization Failed");
//         neopixelWrite(RGB_LED, 255,0,0); //red led if radio failed
//         delay(1000);
//         neopixelWrite(RGB_LED, 0,0,0);
//         while (1);
//     }
//     // neopixelWrite(RGB_BUILTIN, 0,255,0); //green led if radio success
//     // delay(1000);
//     // neopixelWrite(RGB_BUILTIN, 0,0,0);
//     Serial.print("Second Radio Initialized from core: ");
//     Serial.println(xPortGetCoreID());

//     if (!rfm95w.setFrequency(RFM95W_FREQ)) {
//         Serial.println("Second setFrequency Failed");
//         while (1);
//     }
//     Serial.print("Second Frequency set to: "); Serial.println(RFM95W_FREQ);

//     rfm95w.setTxPower(23, false);

//     for(;;) {
//         if (rfm95w.available())
//         {
//             Serial.println("RECEIVER IS AVAILABLE");
//             // Should be a message for us now   
//             uint8_t buf_rx[RH_RF95_MAX_MESSAGE_LEN];
//             uint8_t len = sizeof(buf_rx);
//             if (rfm95w.recv(buf_rx, &len)) 
//             {
//                 RH_RF95::printBuffer("Received: ", buf_rx, len);
//                 Serial.print("Got: ");
//                 Serial.println((char*)buf_rx);
//                 Serial.print("RSSI: ");
//                 Serial.println(rfm95w.lastRssi(), DEC);

//                 // delay(5000);

//                 // // Send a reply
//                 // uint8_t data[] = "And hello back to you";
//                 // rfm95w.send(data, sizeof(data));
//                 // rfm95w.waitPacketSent();
//                 // Serial.println("Sent a reply");
//             }
//             else
//             {
//                 Serial.println("Receive failed");
//             }
//             break;
//         } 
//         // else {
//         //     Serial.println("Receiver NOT AVAILABLE");
//         // }
//         delay(1000);
//     }
// }

RH_RF95 rfm95w(RFM95W_CS, RFM95W_INT);

void setup() {

    pinMode(RFM95W_RST, OUTPUT);
    pinMode(RFM95W_EN, OUTPUT);
    pinMode(RGB_LED, OUTPUT);

    digitalWrite(RFM95W_RST, HIGH);
    digitalWrite(RFM95W_EN, HIGH);
   
    Serial.begin(9600);
    delay(1000);
    if (Serial){
        neopixelWrite(RGB_BUILTIN, 0,0,255); // blue led if serial initialized
        delay(1000);
        neopixelWrite(RGB_BUILTIN, 0,0,0);
        delay(1000);
    }

    // manual reset
    digitalWrite(RFM95W_RST, LOW);
    delay(10);
    digitalWrite(RFM95W_RST, HIGH);
    delay(10);

    while (!rfm95w.init()) {
        Serial.println("First Radio Initialization Failed");
        neopixelWrite(RGB_LED, 255,0,0); //red led if radio failed
        delay(1000);
        neopixelWrite(RGB_LED, 0,0,0);
        while (1);
    }
    neopixelWrite(RGB_BUILTIN, 0,255,0); //green led if radio success
    delay(1000);
    neopixelWrite(RGB_BUILTIN, 0,0,0);

    Serial.print("First Radio Initialized from core: ");
    // Serial.println(xPortGetCoreID());

    delay(1000);

    if (!rfm95w.setFrequency(RFM95W_FREQ)) {
        Serial.println("First setFrequency Failed");
        while (1);
    }
    Serial.print("First frequency set to: "); Serial.println(RFM95W_FREQ);

    rfm95w.setTxPower(23, false);

    rfm95w.setModemConfig(RH_RF95::Bw125Cr48Sf512);

    neopixelWrite(RGB_LED, 0,255,0); //green if frequency and power set
    delay(1000);                      
    neopixelWrite(RGB_LED, 0,0,0);

    Serial.println("Sending message to rf95 server");

    char radiopacket[20] = "Hello There !      ";
    itoa(packetnum++, radiopacket+13, 10);
    Serial.print("Sending "); Serial.println(radiopacket);
    radiopacket[19] = 0;
    
    Serial.println("Sending..."); delay(10);
    rfm95w.send((uint8_t *)radiopacket, 20);

    Serial.println("Waiting for packet to complete..."); delay(10);
    rfm95w.waitPacketSent();
    Serial.println("Packet sent!!");

    // delay(1000);

    neopixelWrite(RGB_LED, 0,255,0); //message sent
    delay(1000);
    neopixelWrite(RGB_LED, 0,0,0);
}


void loop() {

    // Serial.println("Sending message to rf95 server");

    // char radiopacket[20] = "Hello World #      ";
    // itoa(packetnum++, radiopacket+13, 10);
    // Serial.print("Sending "); Serial.println(radiopacket);
    // radiopacket[19] = 0;
    
    // Serial.println("Sending..."); delay(10);
    // rfm95w.send((uint8_t *)radiopacket, 20);

    // Serial.println("Waiting for packet to complete..."); delay(10);
    // rfm95w.waitPacketSent();
    // Serial.println("Packet sent!!");
    // Now wait for a reply

    // Should be a message for us now   
    // uint8_t buf_rx[RH_RF95_MAX_MESSAGE_LEN];
    // uint8_t len = sizeof(buf_rx);
        
    // if (rfm95w2.available())
    // {
    //     if (rfm95w2.recv(buf_rx, &len)) 
    //     {
    //         RH_RF95::printBuffer("Received: ", buf_rx, len);
    //         Serial.print("Got: ");
    //         Serial.println((char*)buf_rx);
    //         Serial.print("RSSI: ");
    //         Serial.println(rfm95w2.lastRssi(), DEC);

    //         // Send a reply
    //         uint8_t data[] = "And hello back to you";
    //         rfm95w2.send(data, sizeof(data));
    //         rfm95w2.waitPacketSent();
    //         Serial.println("Sent a reply");
    //     }
    //     else
    //     {
    //         Serial.println("Receive failed");
    //     }
    // }

    // uint8_t buf_tx[RH_RF95_MAX_MESSAGE_LEN];
    // uint8_t len_tx = sizeof(buf_tx);
    // Serial.println("Waiting for reply..."); delay(10);
    // if (rfm95w.waitAvailableTimeout(10000))
    // { 
    //     // Should be a reply message for us now   
    //     if (rfm95w.recv(buf_tx, &len_tx))
    //     {
    //         Serial.print("Got reply: ");
    //         Serial.println((char*)buf_tx);
    //         Serial.print("RSSI: ");
    //         Serial.println(rfm95w.lastRssi(), DEC);    
    //     }
    //     else
    //     {
    //         Serial.println("Receive failed");
    //     }
    // }
    // else
    // {
    //     Serial.println("No reply, is there a listener around?");
    // }

    // delay(5000);
}

    ////////////////////////////////

    // while (!rfm95w2.init()) {
    //     Serial.println("Second Radio Initialization Failed");
    //     neopixelWrite(RGB_LED, 255,0,0); //red led if radio failed
    //     delay(1000);
    //     neopixelWrite(RGB_LED, 0,0,0);
    //     while (1);
    // }
    // neopixelWrite(RGB_BUILTIN, 0,255,0); //green led if radio success
    // delay(1000);
    // neopixelWrite(RGB_BUILTIN, 0,0,0);
    // Serial.print("Second Radio Initialized from core: ");
    // Serial.println(xPortGetCoreID());

    // if (!rfm95w2.setFrequency(RFM95W_FREQ)) {
    //     Serial.println("Second setFrequency Failed");
    //     while (1);
    // }
    // Serial.print("Second Frequency set to: "); Serial.println(RFM95W_FREQ);

    // rfm95w2.setTxPower(23, false);

    /////////////////////////////////
 
    // delay(3000);

    // xTaskCreatePinnedToCore(threadOne, "Transceive", 20000, NULL, 1, &Task1, 0);
    // delay(500);
    // xTaskCreatePinnedToCore(threadTwo, "Receive", 20000, NULL, 1, &Task2, 1);