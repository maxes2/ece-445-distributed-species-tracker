#include <Arduino.h>
#include <Wire.h>
#include "gps.h"

#define GPS_SCL 7
#define GPS_SDA 6

GPS gps;

void GPS::init() {
    latitude = 10.0;
    longitude = 10.0;
    altitude = 10.0;

    // Wire.begin(GPS_SDA, GPS_SCL);

    // if (!GNSS.begin(Wire)) {
    //     Serial.println("Failed to connect to GPS");
    // }
    // GNSS.setI2COutput(COM_TYPE_UBX);

}

void GPS::updateData() {
    // if (GNSS.getPVT()) {
    //     latitude = (float)(GNSS.getLatitude()/10000000.0);
    //     longitude = (float)(GNSS.getLongitude()/10000000.0);
    //     altitude = GNSS.getAltitudeMSL();
    // }

    latitude = 40.115278;
    longitude = -88.228367;
    altitude = 237.744;
}

float GPS::getLongitude() const {
    return longitude;
}

float GPS::getLatitude() const {
    return latitude;
}

float GPS::getAltitude() const {
    return altitude;
}