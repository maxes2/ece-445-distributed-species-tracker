#include <Arduino.h>
#include <SPI.h>
#include <RH_RF95.h>

#define RFM95W_RST 35
#define RFM95W_EN 1
#define RFM95W_INT 14
#define RFM95W_CS 10

#define RGB_LED 48
#define RFM95W_FREQ 915.0

RH_RF95 rfm95w(RFM95W_CS, RFM95W_INT);

void setup() 
{
  pinMode(RFM95W_RST, OUTPUT);
  pinMode(RFM95W_EN, OUTPUT);
  // pinMode(RFM95W_RST2, OUTPUT);
  // pinMode(RFM95W_EN2, OUTPUT);
  pinMode(RGB_LED, OUTPUT);

  digitalWrite(RFM95W_RST, HIGH);
  digitalWrite(RFM95W_EN, HIGH);

  Serial.begin(9600);
  delay(1000);
  if (Serial){
      neopixelWrite(RGB_BUILTIN, 0,0,255); // blue led if serial initialized
      delay(1000);
      neopixelWrite(RGB_BUILTIN, 0,0,0);
      delay(1000);
  }

  digitalWrite(RFM95W_RST, LOW);
  delay(10);
  digitalWrite(RFM95W_RST, HIGH);
  delay(10);

  
  if (!rfm95w.init()) {
    Serial.println("Init error");
    neopixelWrite(RGB_LED, 255,0,0); //red led if radio failed
    delay(1000);
    neopixelWrite(RGB_LED, 0,0,0);
    while (1);
  }

  Serial.println("Init success !!");

  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  if (!rfm95w.setFrequency(RFM95W_FREQ)) {
    Serial.println("Error setting freq");
    while (1);
  }

  Serial.print("Successfully set frequency: ");
  Serial.print(RFM95W_FREQ);

  // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on

  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then 
  // you can set transmitter powers from 5 to 23 dBm:
  rfm95w.setTxPower(23, false);
  Serial.print("Successfully set power: ");

  rfm95w.setModemConfig(RH_RF95::Bw125Cr48Sf512);

}


void loop()
{
  if (rfm95w.available())
  {
    // Should be a message for us now   
    uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
    uint8_t len = sizeof(buf);
    
    if (rfm95w.recv(buf, &len))
    {
        RH_RF95::printBuffer("Received: ", buf, len);
        Serial.print("Got: ");
        Serial.println((char*)buf);
        Serial.print("RSSI: ");
        Serial.println(rfm95w.lastRssi(), DEC);
        Serial.print("SNR: ");
        Serial.println(rfm95w.lastSNR(), DEC);

        // Send a reply
        // uint8_t data[] = "And hello back to you";
        // rfm95w.send(data, sizeof(data));
        // rfm95w.waitPacketSent();
        // Serial.println("Sent a reply");
    }
    else
    {
        Serial.println("Receive failed");
    }
  }
}
