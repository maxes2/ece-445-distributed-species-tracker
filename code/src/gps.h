#pragma once

#include "SparkFun_u-blox_GNSS_v3.h"

struct GPS {
    public:
        GPS() = default;
        void init();
        void updateData();
        float getLatitude() const;
        float getLongitude() const;
        float getAltitude() const;

    private:
        SFE_UBLOX_GNSS GNSS;
        float latitude;
        float longitude;
        float altitude;
};