#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <EEPROM.h>
#include <WiFi.h>
#include "peripherals.h"

#define IR_PIN 5
#define RGB_LED 48
#define JETSON_OUT 37
#define JETSON_YES 39
#define JETSON_NO 41

// #define BASE_NODE

TaskHandle_t Task1;
TaskHandle_t Task2;

// GPS gps;

void threadOne(void * pvParameters) {
    // int address = 0;
    // // delay(3000);
    // if (!sighting_map.empty()) {
    //     Serial.println("ALL OF OUR DATA: ");
    //     for (auto const& elem : sighting_map) {
    //         NetworkPacket data;
    //         EEPROM.get(address, data);
    //         address += sizeof(data);
    //         Serial.print("Sighting Location : Node ");
    //         Serial.println(data.id);
    //         Serial.printf("GPS data: %.3f", data.gps_lat);
    //         Serial.println(" ");
    //         Serial.printf("GPS data: %.3f", data.gps_long);
    //         Serial.println(" ");
    //         Serial.printf("GPS data: %.3f", data.gps_alt);
    //         Serial.println(" ");
    //         Serial.print("Animal Sighted : ");
    //         Serial.println((data.animal).c_str());
    //         Serial.println("  ");
    //     }
    // }

    while(1) {
        network.receive();
        vTaskDelay(100);
    }
}

void threadTwo(void * pvParameters) {
    // final version
    while(1) {
        if (digitalRead(IR_PIN) == HIGH) {
            neopixelWrite(RGB_LED,230,230,250); // lavender if detected
            delay(500);
            neopixelWrite(RGB_LED,0,0,0);

            digitalWrite(JETSON_OUT, LOW);
            // Serial.println("Lowered pin");
            delay(300);
            digitalWrite(JETSON_OUT, HIGH);
            // Serial.println("Raised pin");
            delay(1000);
            digitalWrite(JETSON_OUT, LOW);

            // unsigned long detect_time = millis();
            // Serial.println("Detected something!");

            // gps.updateData();
            // Serial.println("Updated GPS data");

            // unsigned long detect_to_send_latency = millis() - detect_time;
            // const char* animal = "human";
            // network.transmit_data(animal);
            // unsigned long detect_to_send_latency = network.message_sent_time - detect_time;
            // Serial.print("Detect-to-send latency: ");
            // Serial.println(detect_to_send_latency);
            // Serial.println("Done with transmit data");

            unsigned long detect_time = millis();
            while(1) {
                if (millis() - detect_time > 10000) {
                    Serial.println("Jetson didn't respond in time");
                    break;
                }
                // Serial.println("Waiting for Jetson");
                if (digitalRead(JETSON_YES) == HIGH) {
                    const char* animal = "human";
                    unsigned long picture_sent_time = millis();
                    // Serial.println("Jetson has a positive classification!!");

                    gps.updateData();
                    // Serial.println("Updated GPS data");

                    // unsigned long message_sent_time = millis();
                    network.transmit_data(animal);
                    unsigned long detect_to_send_latency = network.message_sent_time - detect_time;
                    unsigned long picture_to_send_latency = network.message_sent_time - picture_sent_time;
                    Serial.print("Detect-to-send latency: ");
                    Serial.println(detect_to_send_latency);
                    Serial.print("Picture-to-send latency: ");
                    Serial.println(picture_to_send_latency);
                    // delay(4000);
                    // Serial.println("Done with transmit data");
                    break;
                } else if (digitalRead(JETSON_NO) == HIGH) {
                    Serial.println("Jetson says it's not a correct image");
                    break;
                }
            }
            // vTaskDelay(3000);
        }
        vTaskDelay(3000);
    }
    // end final version
}

void setup() {
    Serial.begin(9600);
    delay(1000);
    if (Serial){
        neopixelWrite(RGB_BUILTIN, 0,0,255); // blue led if serial initialized
        delay(1000);
        neopixelWrite(RGB_BUILTIN, 0,0,0);
        delay(1000);
    }

    pinMode(IR_PIN, INPUT);
    digitalWrite(IR_PIN, LOW);
    // pinMode(JETSON_NO, INPUT);
    // pinMode(JETSON_YES, INPUT);
    pinMode(JETSON_OUT, OUTPUT);
    Serial.println("Set the pin modes for Jetson");

    // SPI.begin(JETSON_SCK, JETSON_MISO, JETSON_MOSI, JETSON_CS);

    network.init();
    neopixelWrite(RGB_LED, 0,255,0); //green led if radio success
    delay(1000);
    neopixelWrite(RGB_LED, 0,0,0);
    // Serial.println("Radio initalized");

    gps.init();
    Serial.println("GPS initialized");

    // pinMode(IR_PIN, INPUT);

    xTaskCreatePinnedToCore(threadOne, "Receive", 20000, NULL, 1, &Task1, 0);
    // delay(500);
    xTaskCreatePinnedToCore(threadTwo, "PrepareAndSendData", 20000, NULL, 1, &Task2, 1);
}

void loop() {

}