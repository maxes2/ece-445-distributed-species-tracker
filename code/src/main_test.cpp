#include <Arduino.h>

#define RGB_LED 48

void setup() {
  Serial.begin(9600); //
  pinMode(RGB_LED, OUTPUT); //set the LED pin as an output
  Serial.println("Starting to blink");
  for (int i = 0; i < 5; i++) {  //flash the LED 5 times
    // Serial.println("Turning ON long");
    // neopixelWrite(RGB_LED, 255,0,0);  //turn the LED on
    // delay(1000);                //wait for 500 milliseconds
    // Serial.println("Turning OFF long");
    // neopixelWrite(RGB_LED, 0,0,0);   //turn the LED off
    // delay(1000);                //wait for another 500 milliseconds
    neopixelWrite(RGB_LED,230,230,250); // lavender if detected
    delay(1000);
    neopixelWrite(RGB_LED,0,0,0);
    delay(500);
    neopixelWrite(RGB_LED,255,165,0); // orange if received
    delay(1000);
    neopixelWrite(RGB_LED,0,0,0);
    delay(500);
  }
  // for (int i = 0; i < 5; i++) {  //flash the LED 5 times
  //   Serial.println("Turning ON short");
  //   neopixelWrite(RGB_LED, 0,255,0);  //turn the LED on
  //   delay(500);                //wait for 500 milliseconds
  //   Serial.println("Turning OFF short");
  //   neopixelWrite(RGB_LED, 0,0,0);   //turn the LED off
  //   delay(500);                //wait for another 500 milliseconds
  // }
}

void loop() {

}