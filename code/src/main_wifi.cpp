/*
 WiFi Web Server LED Blink
 A simple web server that lets you blink an LED via the web.
 This sketch will print the IP address of your WiFi Shield (once connected)
 to the Serial monitor. From there, you can open that address in a web browser
 to turn on and off the LED on pin 5.
 If the IP address of your shield is yourAddress:
 http://yourAddress/H turns the LED on
 http://yourAddress/L turns it off
 This example is written for a network using WPA2 encryption. For insecure
 WEP or WPA, change the Wifi.begin() call and use Wifi.setMinSecurity() accordingly.
 Circuit:
 * WiFi shield attached
 * LED attached to pin 5
 created for arduino 25 Nov 2012
 by Tom Igoe
ported for sparkfun esp32 
31.01.2017 by Jan Hendrik Berlin
 
 */

#include <WiFi.h>
#include <HTTPClient.h>

#define RGB_LED 48

// const char* ssid     = "Maxspot";
// const char* password = "ybtd4773";

const char* ssid     = "A Boston Song";
const char* password = "foodrelated";

WiFiServer server(80);

void setup()
{
    Serial.begin(115200);
    pinMode(RGB_LED, OUTPUT);      // set the LED pin mode

    delay(10);

    // We start by connecting to a WiFi network

    // Serial.println();
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);

    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }

    Serial.println("");
    Serial.println("WiFi connected.");
    Serial.print("IP address: ");
    Serial.println(WiFi.localIP());
    
    // server.begin();
    HTTPClient http;
    String serverUrl = "http://192.168.0.143:8000/api/messages";
    http.begin(serverUrl);
    http.addHeader("Content-Type", "application/json");
    String animal_name = "human";

    String jsonMessage1 = "{\"message\":\"Node: " + String(1) + ", Animal Identified: " + animal_name + ", GPS: (" + String(40.115278, 6) + "deg, " + String(-88.228367, 6) + "deg, " + String(237.744, 3) + "m), Timestamp: " + String(55946) + "\"}";
    String jsonMessage2 = "{\"message\":\"Node: " + String(2) + ", Animal Identified: " + animal_name + ", GPS: (" + String(40.115278, 6) + "deg, " + String(-88.228367, 6) + "deg, " + String(237.744, 3) + "m), Timestamp: " + String(34467) + "\"}";
    String jsonMessage3 = "{\"message\":\"Node: " + String(3) + ", Animal Identified: " + animal_name + ", GPS: (" + String(40.115278, 6) + "deg, " + String(-88.228367, 6) + "deg, " + String(237.744, 3) + "m), Timestamp: " + String(42298) + "\"}";
    String jsonMessage4 = "{\"message\":\"Node: " + String(1) + ", Animal Identified: " + animal_name + ", GPS: (" + String(40.115278, 6) + "deg, " + String(-88.228367, 6) + "deg, " + String(237.744, 3) + "m), Timestamp: " + String(110370) + "\"}";
    String jsonMessage5 = "{\"message\":\"Node: " + String(2) + ", Animal Identified: " + animal_name + ", GPS: (" + String(40.115278, 6) + "deg, " + String(-88.228367, 6) + "deg, " + String(237.744, 3) + "m), Timestamp: " + String(79456) + "\"}";
    String jsonMessage6 = "{\"message\":\"Node: " + String(3) + ", Animal Identified: " + animal_name + ", GPS: (" + String(40.115278, 6) + "deg, " + String(-88.228367, 6) + "deg, " + String(237.744, 3) + "m), Timestamp: " + String(124539) + "\"}";
    String jsonMessage7 = "{\"message\":\"Node: " + String(2) + ", Animal Identified: " + animal_name + ", GPS: (" + String(40.115278, 6) + "deg, " + String(-88.228367, 6) + "deg, " + String(237.744, 3) + "m), Timestamp: " + String(209456) + "\"}";


    Serial.println("Sending data to server");
    int httpCode = http.POST(jsonMessage1);
    if (httpCode > 0)
    {
      String response = http.getString();
      Serial.println("Server response: " + response);
    }
    else
    {
      Serial.printf("Error sending data to server: %d\n", httpCode);
    }

    httpCode = http.POST(jsonMessage2);
    if (httpCode > 0)
    {
      String response = http.getString();
      Serial.println("Server response: " + response);
    }
    else
    {
      Serial.printf("Error sending data to server: %d\n", httpCode);
    }

    httpCode = http.POST(jsonMessage3);
    if (httpCode > 0)
    {
      String response = http.getString();
      Serial.println("Server response: " + response);
    }
    else
    {
      Serial.printf("Error sending data to server: %d\n", httpCode);
    }

    httpCode = http.POST(jsonMessage4);
    if (httpCode > 0)
    {
      String response = http.getString();
      Serial.println("Server response: " + response);
    }
    else
    {
      Serial.printf("Error sending data to server: %d\n", httpCode);
    }

    httpCode = http.POST(jsonMessage5);
    if (httpCode > 0)
    {
      String response = http.getString();
      Serial.println("Server response: " + response);
    }
    else
    {
      Serial.printf("Error sending data to server: %d\n", httpCode);
    }

    httpCode = http.POST(jsonMessage6);
    if (httpCode > 0)
    {
      String response = http.getString();
      Serial.println("Server response: " + response);
    }
    else
    {
      Serial.printf("Error sending data to server: %d\n", httpCode);
    }

    httpCode = http.POST(jsonMessage7);
    if (httpCode > 0)
    {
      String response = http.getString();
      Serial.println("Server response: " + response);
    }
    else
    {
      Serial.printf("Error sending data to server: %d\n", httpCode);
    }

    http.end();
}

void loop(){

      
//  WiFiClient client = server.available();   // listen for incoming clients

//   if (client) {                             // if you get a client,
//     Serial.println("New Client.");           // print a message out the serial port
//     String currentLine = "";                // make a String to hold incoming data from the client
//     while (client.connected()) {            // loop while the client's connected
//       if (client.available()) {             // if there's bytes to read from the client,
//         char c = client.read();             // read a byte, then
//         Serial.write(c);                    // print it out the serial monitor
//         if (c == '\n') {                    // if the byte is a newline character

//           // if the current line is blank, you got two newline characters in a row.
//           // that's the end of the client HTTP request, so send a response:
//           if (currentLine.length() == 0) {
//             // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
//             // and a content-type so the client knows what's coming, then a blank line:
//             client.println("HTTP/1.1 200 OK");
//             client.println("Content-type:text/html");
//             client.println();

//             // the content of the HTTP response follows the header:
//             client.print("Click <a href=\"/H\">here</a> to turn the LED on pin 48 on.<br>");
//             client.print("Click <a href=\"/L\">here</a> to turn the LED on pin 48 off.<br>");

//             // The HTTP response ends with another blank line:
//             client.println();
//             // break out of the while loop:
//             break;
//           } else {    // if you got a newline, then clear currentLine:
//             currentLine = "";
//           }
//         } else if (c != '\r') {  // if you got anything else but a carriage return character,
//           currentLine += c;      // add it to the end of the currentLine
//         }

//         // Check to see if the client request was "GET /H" or "GET /L":
//         if (currentLine.endsWith("GET /H")) {
//           neopixelWrite(RGB_LED, 0,255,0);               // GET /H turns the LED on
//         }
//         if (currentLine.endsWith("GET /L")) {
//           neopixelWrite(RGB_LED, 0,0,0);             // GET /L turns the LED off
//         }
//       }
//     }
//     // close the connection:
//     client.stop();
//     Serial.println("Client Disconnected.");
//   }
}
