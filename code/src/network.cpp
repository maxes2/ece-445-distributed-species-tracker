#include <Arduino.h>
#include <SPI.h>
#include <RH_RF95.h>
#include <RHMesh.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include "network.h"
#include "gps.h"
#include "peripherals.h"
#include <map>
// #include <EEPROM.h>
// #include <RTClib.h>

#define RFM95W_RST 35
#define RFM95W_EN 1
#define RFM95W_INT 14
#define RFM95W_CS 10

// #define RFM95W_RST2 41
// #define RFM95W_EN2 5
// #define RFM95W_CS2 42
// #define RFM95W_INT2 9

#define RGB_LED 48
#define RFM95W_FREQ 915.0

#define BASE_NODE

Network network;
// std::map<int, NetworkPacket> sighting_map;
int sighting_count = 0;
int address = 0;
SemaphoreHandle_t transceiverMutex;

std::vector<NetworkPacket> msg_list;
std::vector<NetworkPacket> left_to_publish;

std::vector<int> mem_list;

int message_cnt= 0;

const char* ssid = "";
const char* password = "";
String local_IP = ""; 
bool wifi_first = true;

Network::Network() : rfm95w(RFM95W_CS, RFM95W_INT), mesh(rfm95w, MY_ADDR) {}

uint8_t in_buf[RH_MESH_MAX_MESSAGE_LEN];

void Network::init() {
    pinMode(RFM95W_RST, OUTPUT);
    digitalWrite(RFM95W_RST, HIGH);
    delay(100);

    digitalWrite(RFM95W_RST, LOW);
    delay(10);
    digitalWrite(RFM95W_RST, HIGH);
    delay(10);

    while (!rfm95w.init()) {
        Serial.println("Radio Initialization Failed");
        neopixelWrite(RGB_LED, 255,0,0); //red led if radio failed
        delay(1000);
        neopixelWrite(RGB_LED, 0,0,0);
        while (1);
    }
    Serial.println("Radio Initialized");

    // if (!rfm95w.setFrequency(RFM95W_FREQ)) {
    //     while (true) {
    //         Serial.println("ERROR: Default setFrequency Failed");
    //     }
    // }
    if (!rfm95w.setFrequency(RFM95W_FREQ)) {
        Serial.println("First setFrequency Failed");
        while (1);
    }

    rfm95w.setTxPower(23, false);

    // rfm95w.setModemConfig(RH_RF95::Bw125Cr48Sf4096);
    rfm95w.setModemConfig(RH_RF95::Bw125Cr45Sf128);

    transceiverMutex = xSemaphoreCreateMutex();
}

void printError(uint8_t error) {
  switch(error) {
    case 1: Serial.println("invalid length");
    break;
    case 2: Serial.println("no route");
    break;
    case 3: Serial.println("timeout");
    break;
    case 4: Serial.println("no reply");
    break;
    case 5: Serial.println("unable to deliver");
    break;
  }
}

void Network::transmit_data(const char* animal) {
    NetworkPacket packet = makePacket(animal);
    // Serial.println("Adding data that we've gathered");
    addDataToList(packet);

    if (xSemaphoreTake(transceiverMutex, portMAX_DELAY) == pdTRUE) {
        for (int i = 1; i <= 3; i++) {
            int dest = i;
            int dest = mem_list[i];
            if (dest == MY_ADDR) {
                Serial.println("Not gonna send to my address again");
                continue;
            }

            // Serial.println("Sending message to network");
            message_sent_time = millis();
            uint8_t result = mesh.sendtoWait((uint8_t*)&packet, sizeof(packet), i);
            if (result != RH_ROUTER_ERROR_NONE) {
                Serial.println("NOOOO ERROR!!");
                printError(result);
                // auto it = std::find(mem_list.begin(), mem_list.end(), dest);
                // mem_list.erase(it);
                // Serial.print("Couldn't transmit message to: ");
                // Serial.println(dest);
                // continue;
            } else {
                Serial.println("MESSAGE SENT");
            }
        }
        #ifdef BASE_NODE
        publishData();
        #endif
        xSemaphoreGive(transceiverMutex);
    }
}

// void Network::join() {
//     mem_list.push_back(MY_ADDR);
//     // if (mem_list.size() == 1) {
//     //     Serial.println("I'm the only one here so far");
//     //     return;
//     // }

//     NetworkPacket packet = makeJoinPacket(1);
//     if (xSemaphoreTake(transceiverMutex, portMAX_DELAY) == pdTRUE) {
//         uint8_t result = mesh.sendtoWait((uint8_t*)&packet, sizeof(packet), RH_BROADCAST_ADDRESS);
//         if (result != RH_ROUTER_ERROR_NONE) {
//             Serial.println("error sending my address to join");
//             printError(result);
//         } else {
//             Serial.println("Sent my join message");
//         }
//         xSemaphoreGive(transceiverMutex);
//     }
// }

void Network::receive() {
    if (xSemaphoreTake(transceiverMutex, portMAX_DELAY) == pdTRUE) {
        // Serial.println("In the receive locked area");
        uint8_t len = sizeof(in_buf);
        uint8_t from;
        if (mesh.recvfromAck(in_buf, &len, &from)) {
            NetworkPacket recv_packet;
            Serial.print("GOT A PACKET FROM: ");
            Serial.println(from);
            neopixelWrite(RGB_LED,255,165,0); // orange if received
            delay(500);
            neopixelWrite(RGB_LED,0,0,0);
            memcpy(&recv_packet, in_buf, sizeof(recv_packet));

            // if (recv_packet.join == 0) {
            Serial.println("Adding data that we've received");
            addDataToList(recv_packet);

            Serial.println("Returned from add data to list");
            #ifdef BASE_NODE
            publishData();
            #endif
            // }
            // else if(recv_packet.join == 1) {
            //     Serial.println("Getting a join message");
            //     mem_list.push_back(recv_packet.id);
            //     for (int i = 0; i < mem_list.size(); i++) {
            //         Serial.print(mem_list[i]);
            //         Serial.println(" ");
            //     }

            //     NetworkPacket packet = makeJoinPacket(2);
            //     uint8_t result = mesh.sendtoWait((uint8_t*)&packet, sizeof(packet), RH_BROADCAST_ADDRESS);
            //     if (result != RH_ROUTER_ERROR_NONE) {
            //         Serial.println("error sending updated mem list back");
            //         printError(result);
            //     } else {
            //         Serial.println("Sent my join message");
            //     }

            //     return;
            // }
            // else if (recv_packet.join == 2) {
            //     mem_list = recv_packet.members;
            //     Serial.println("Getting updated mem list: ");
            //     for (int i = 0; i < mem_list.size(); i++) {
            //         Serial.print(mem_list[i]);
            //         Serial.println(" ");
            //     }
            // }
        }
        xSemaphoreGive(transceiverMutex);
    }
}

void Network::addDataToList(NetworkPacket data) {
    // sighting_count++;
    // sighting_map[sighting_count] = data;
    // for (int i = 0; i < sighting_count; i++) {
    //     EEPROM.put(address, sighting_map[i]);
    //     address += sizeof(sighting_map[i]);
    // }
    // EEPROM.commit();
    Serial.println("New packet!");
    msg_list.push_back(data);
    left_to_publish.push_back(data);

    for (int i = 0; i < msg_list.size(); i++) {
        NetworkPacket d = msg_list[i];
        Serial.print("Sighting Location : Node ");
        Serial.println(d.id);
        Serial.printf("GPS data: %.3f", d.gps_lat);
        Serial.println(" ");
        Serial.printf("GPS data: %.3f", d.gps_long);
        Serial.println(" ");
        Serial.printf("GPS data: %.3f", d.gps_alt);
        Serial.println(" ");
        Serial.print("Animal Sighted : ");
        Serial.println(d.animal);
        Serial.print("Timestamp: ");
        Serial.println(d.time);
        Serial.print("Message number: ");
        Serial.println(d.message_num);
        Serial.println(" ");
    }
}

void Network::publishData() {

    if (wifi_first) {
        Serial.println();
        Serial.println();
        Serial.print("Connecting to ");
        Serial.println(ssid);
        WiFi.begin(ssid, password);

        unsigned long wifi_wait = millis();
        while (WiFi.status() != WL_CONNECTED) {
            if (millis() - wifi_wait > 5000) {
                Serial.println("Couldn't connet to wifi");
                return;
            }
            delay(500);
            Serial.print(".");
        }

        Serial.println("");
        Serial.println("WiFi connected.");
        Serial.print("IP address: ");
        Serial.println(WiFi.localIP());
        wifi_first = false;
    }
    
    // server.begin();
    HTTPClient http;
    String serverUrl = "http://"+local_IP+":8000/api/messages";
    http.begin(serverUrl);
    http.addHeader("Content-Type", "application/json");

    for (int i = 0; i < left_to_publish.size(); i++) {
        NetworkPacket data = left_to_publish[i];
        String jsonMessage = "{\"message\":\"Node: " + String(data.id) + ", Animal: " + String(data.animal) + ", GPS: (" + String(data.gps_lat, 6) + "deg, " + String(data.gps_long, 6) + "deg, " + String(data.gps_alt, 2) + " m), Timestamp: " + String(data.time) + "\"}";
        Serial.println("Sending data to server");
        int httpCode = http.POST(jsonMessage);

        if (httpCode > 0)
        {
            String response = http.getString();
            Serial.println("Server response: " + response);
            left_to_publish.erase(left_to_publish.begin());
            i--;
        }
        else
        {
            Serial.printf("Error sending data to server: %d\n", httpCode);
        }
    }
    http.end();
}


NetworkPacket Network::makePacket(const char* animal) {
    NetworkPacket packet {};
    packet.id = MY_ADDR;
    packet.gps_lat = gps.getLatitude();
    packet.gps_long = gps.getLongitude();
    packet.gps_alt = gps.getAltitude();
    strncpy(packet.animal, animal, strlen(animal));
    packet.time = millis();
    packet.message_num = message_cnt++;
    // packet.join = 0;
    // packet.members = mem_list;
    // packet.rssi = rfm95w.lastRssi();
    return packet;
}

// NetworkPacket Network::makeJoinPacket(int type) {
//     NetworkPacket packet {};
//     packet.id = MY_ADDR;
//     packet.gps_lat = 0;
//     packet.gps_long = 0;
//     packet.gps_alt = 0;
//     strncpy(packet.animal, "human", 6);
//     packet.message_num = 0;
//     packet.join = type;
//     packet.members = mem_list;
//     // packet.time = 
//     // packet.rssi = rfm95w.lastRssi();
//     return packet;
// }

