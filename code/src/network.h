#pragma once

#include <RH_RF95.h>
#include <RHMesh.h>
#include <string>
#include <map>
#include <vector>
// #include <data.h>

#define RFM95W_FREQ 915.0
#define NETWORK_SIZE 3
#define MY_ADDR 3

class Network;
// extern Network network;

struct NetworkPacket {
    int id;
    float gps_lat;
    float gps_long;
    float gps_alt;
    char animal[6];
    int message_num;
    unsigned long time;
    //  int join;
    //  std::vector<int> members;
    // int8_t rssi;
};

class Network {
   public:
        Network();

        void init();

        void transmit_data(const char* animal);

        void receive();

        unsigned long message_sent_time;

     //    void join();

     //    void handle_join();

   private:
        RH_RF95 rfm95w;
        RHMesh mesh;

        NetworkPacket makePacket(const char* animal);
     //    NetworkPacket makeJoinPacket(int type);
        void addDataToList(NetworkPacket new_data);
        void publishData();
};